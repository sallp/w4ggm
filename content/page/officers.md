 
+++
date = "2019-01-15"
title = "Officers"
 
+++


#### President - Gerald Harris W4TOT
![W4TOT](/img/W4TOT_gerald.jpg)


#### Vice President - Terry Boatright KG4FNG
![KG4FNG](/img/KG4FNG_terry.jpg)


#### Treasurer - Judy Smith
![SMITH](/img/SMITH_judy.jpg)


#### Secretary - Dave Tipton W5DMT
![W5DMT](/img/W5DMT_dave.jpg)


#### Emergency Coordinator - Bill Reed W4WRR
![W4WRR](/img/W4WRR_bill.jpg)


#### VE Testing/Field Day Coordinator - Paul Kemp WB0CJB
![WBOCJB](/img/WB0CJB_paul.jpg)


#### Newsletter Editor - Bill Milligan K4BX
![K4BX](/img/K4BX_bill.jpg)


#### Repeater Guru - John Hartman N5AAA
![N5AAA](/img/N5AAA_john.jpg)


#### Photographer - Ralph Patterson KG4CPI
![KG4CPI](/img/KG4CPI_ralph.jpg)


#### Webmaster - Dale Adams KA4ZDR
![BIGDOG](/img/KA4ZDR_dale.jpg)
