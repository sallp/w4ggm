 
+++
date = "2019-01-15"
title = "Amateur Radio Repeaters"
 
+++
 
### Maury County, Columbia, TN And Surrounding Areas

# Maury County

| Frequency     | License     | Type | Tone Out | Tone In | Alpha Tag | Description                     |
| ------------- |-------------|------|----------|---------|-----------|---------------------------------|
| 147.12000     | W4GGM       | RM   | 127.3    |         | W4GGM 2M  | Columbia 2 Meter Repeater       |
| 443.17500     | W4GGM       | RM   | 100.0    |         | W4GGM 70CM| Columbia 70 Centimeter Repeater |
| 147.28500     | AG4TI       | RM   |          |         | AG4TI     | Culleoka Repeater               |
